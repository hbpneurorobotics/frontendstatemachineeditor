import { useEffect, useRef } from "react";
import { load } from "specification/specification-persistence-module.js";
import { initialState as initialSpecificationState } from "specification";
import { store } from "./../configure-store.js";
import {
  extractSpecification,
  transpileToSmach
} from "./../specification/smach-transpiler-module.js";
import { getSpecification } from "selectors.js";
import watch from "redux-watch";

export function useParentWindowInterfaceCommunication(dispatch) {
  const communicationIsActive = useRef(false);

  // Send a message to the parent
  const sendMessageToParentWindow = function(msg) {
    window.parent.postMessage(msg, "*");
  };

  const handleMessageFromParentWindow = function(e) {
    if (e.data) {
      switch (e.data.command) {
        case "START_NOTIFYING_ME_ON_CHANGES":
          communicationIsActive.current = true;
          break;

        case "STOP_NOTIFYING_ME_ON_CHANGES":
          communicationIsActive.current = false;
          break;

        case "HERE_IS_YOUR_NEW_SPECIFICATION":
          try {
            load(extractSpecification(e.data.payload), dispatch);
          } catch (error) {
            load(JSON.stringify(initialSpecificationState), dispatch);
          }
          break;

        default:
          break;
      }
    }
  };

  // Subwindow interface.
  let w = watch(() => getSpecification(store.getState()));
  store.subscribe(
    w((newVal, oldVal) => {
      if (communicationIsActive.current) {
        sendMessageToParentWindow({
          command: "HERE_IS_THE_NEW_CODE",
          payload: transpileToSmach(store.getState().specification)
        });
      }
    })
  );

  useEffect(() => {
    window.addEventListener("message", handleMessageFromParentWindow);

    return () => {
      window.removeEventListener("message", handleMessageFromParentWindow);
    };
  });
}
