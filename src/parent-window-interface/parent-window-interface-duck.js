import produce from "immer";

// Actions
const SET_ACTIVE = `${appIdentifier}/specification/SET_ACTIVE`;

const initialState = {
  isActive: false
};

// Reducer
export default function reducer(state = initialState, action = {}) {
  return produce((draft, action) => {
    switch (action.type) {
      case SET_ACTIVE:
        draft.isActive = action.isActive;
        break;

      default:
    }
  })(state, action);
}

// Action Creators
export function setActive(isActive) {
  return function(dispatch) {
    dispatch({
      type: SET_ACTIVE,
      isActive
    });
  };
}
