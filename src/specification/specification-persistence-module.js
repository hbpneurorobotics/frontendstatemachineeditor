import { setSpecification } from "./specification-duck.js";

export function serialize(specification) {
  return JSON.stringify(specification);
}

export function load(specificationString, dispatch) {
  // dispatch action
  dispatch(setSpecification(JSON.parse(specificationString)));
}
