const PYTHON_BEGIN_LABEL = "#[PYTHON_BEGIN_LABEL]";
const PYTHON_END_LABEL = "#[PYTHON_END_LABEL]";
const SPECIFICATION_BEGIN_LABEL = "#[SPECIFICATION_BEGIN_LABEL]";
const SPECIFICATION_END_LABEL = "[SPECIFICATION_END_LABEL]";

export function transpileToSmach(specification, imports, prefix) {
  // Python code contsants.
  const firstLineSnippet = "#!/usr/bin/env python";
  const authorSnippet = "__author__ = 'SMACH TRANSPILER'";
  const baseImportsSnippet =
    "import smach_ros\nfrom smach import StateMachine\n";

  let nodeMachineSpecificationSnippet = "";

  const nodes = specification.nodes;
  const edges = specification.edges;

  // Filter for atomic nodes.
  // todo: Resolve hierarchical nodes by forwarding incoming transitions to entry point and adding all outgoing edges to all child nodes.

  let outcomeValues = [];
  let outcomeNodes = [];
  let atomicNodes = [];
  let startNode = undefined;

  for (let i = 0; i < nodes.identifiers.length; i++) {
    const node = nodes.byIdentifier[nodes.identifiers[i]];

    // Only process atomic nodes for now
    if (node.children.length === 0) {
      // Detect outcomes:

      if (node.name === "START") {
        startNode = node;
      } else if (node.smachAction.identifier === "") {
        if (outcomeValues.includes(node.name) === false) {
          outcomeValues.push(node.name);
        }
        outcomeNodes.push(node.identifier);
      } else {
        // normal atomic node
        atomicNodes.push(node);
      }
    }
  }

  // Find start node:
  if (startNode) {
    // Get first state and remove START node:
    const transitionIdentifier = startNode.transitions[0];
    if (transitionIdentifier) {
      const firstNodeIdentifier =
        edges.byIdentifier[transitionIdentifier].to.identifier;
      if (
        atomicNodes.findIndex(
          element => {
            return element.identifier === firstNodeIdentifier
          }
        ) !== -1
      ) {
        atomicNodes.unshift(
          atomicNodes.splice(
            atomicNodes.findIndex(
              element => element.identifier === firstNodeIdentifier
            ),
            1
          )[0]
        );
      }
    }
  }

  for (let i = 0; i < atomicNodes.length; i++) {
    const node = atomicNodes[i];

    if (node.children.length === 0) {
      // For each atomic node get all transitions
      const transitions = node.transitions;
      let relevantEdges = [];
      for (let o = 0; o < transitions.length; o++) {
        const edge = edges.byIdentifier[transitions[o]];
        // Only if this node is the origin
        if (edge.from.identifier === node.identifier) {
          relevantEdges.push(edge);
        }
      }

      let transitionsObjectSnippet = ``;
      for (let o = 0; o < relevantEdges.length; o++) {
        const edge = relevantEdges[o];

        // Check if destination is an outcome
        const toOutcome = outcomeNodes.includes(edge.to.identifier);

        let separator = o < relevantEdges.length - 1 ? ", " : "";
        transitionsObjectSnippet =
          transitionsObjectSnippet +
          `"${edge.if.outcome}": "${
            toOutcome
              ? nodes.byIdentifier[edge.to.identifier].name
              : edge.to.identifier
          }"${separator}`;
      }

      // Add a node snippet for each atomic node:
      const nodeSpecificationSnippet = `    StateMachine.add(
        "${node.identifier}",
        ${node.smachAction.identifier}(${node.smachAction.parameters}),
        transitions={${transitionsObjectSnippet}}
    )
`;

      nodeMachineSpecificationSnippet =
        nodeMachineSpecificationSnippet + nodeSpecificationSnippet;
    }
  }

  let outcomesSnippet = "";
  for (let i = 0; i < outcomeValues.length; i++) {
    outcomesSnippet = `${outcomesSnippet}${i !== 0 ? "," : ""}"${
      outcomeValues[i]
    }"`;
  }

  const baseSnippet = `sm = StateMachine(outcomes=[${outcomesSnippet}])\n\nwith sm:`;

  // Build the final code:
  const result = `
${PYTHON_BEGIN_LABEL}
${firstLineSnippet}

${authorSnippet}

${baseImportsSnippet}

${specification.actionCode}

${baseSnippet}
${nodeMachineSpecificationSnippet}
${PYTHON_END_LABEL}
${SPECIFICATION_BEGIN_LABEL}${JSON.stringify(
    specification
  )}${SPECIFICATION_END_LABEL}
`;

  return result;
}

export function extractSpecification(code) {
  return code
    .split(SPECIFICATION_BEGIN_LABEL)[1]
    .split(SPECIFICATION_END_LABEL)[0];
}
