export const NODE_MIN_WIDTH = 5;
export const NODE_MIN_HEIGHT = 4;
export const DEFAULT_TRANSFORM = {
  minX: 0,
  minY: 0,
  maxX: 1,
  maxY: 1
};
