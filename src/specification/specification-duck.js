import produce from "immer";
import { appIdentifier } from "app-constants.js";
import { not, hasOwnProperty } from "helpers.js";

// Actions
const SET_SPECIFICATION = `${appIdentifier}/specification/SET_SPECIFICATION`;
const SET_ACTION_CODE = `${appIdentifier}/specification/SET_ACTION_CODE`;
const ADD_NODE = `${appIdentifier}/specification/ADD_NODE`;
const REMOVE_NODE = `${appIdentifier}/specification/REMOVE_NODE`;
const UPDATE_NODE = `${appIdentifier}/specification/UPDATE_NODE`;
const SET_NODE_NAME = `${appIdentifier}/specification/SET_NODE_NAME`;
const SET_NODE_SMACH_ACTION = `${appIdentifier}/specification/SET_NODE_SMACH_ACTION`;
const ADD_EDGE = `${appIdentifier}/specification/ADD_EDGE`;
const REMOVE_EDGE = `${appIdentifier}/specification/REMOVE_EDGE`;
const UPDATE_EDGE = `${appIdentifier}/specification/UPDATE_EDGE`;
const SET_EDGE_SMACH_OUTCOME = `${appIdentifier}/specification/SET_EDGE_SMACH_OUTCOME`;

export const initialState = {
  nodes: {
    byIdentifier: {
      start: {
        name: "START",
        identifier: "start",
        parent: "",
        children: [],
        transitions: [],
        entryPoints: {},
        transform: {
          minX: 1,
          maxX: 6,
          minY: 1,
          maxY: 5
        },
        smachAction: {
          identifier: "",
          parameters: ""
        }
      }
    },
    identifiers: ["start"]
  },
  edges: {
    byIdentifier: {
      /*'id1':{
                priority: 1,
                from: '',
                to: '',
                if: {
                    outcome: ''
                },

            }*/
    },
    identifiers: [
      /*id1'*/
    ]
  },
  actionCode: ""
};

// Reducer
export default function reducer(state = initialState, action = {}) {
  return produce((draft, action) => {
    switch (action.type) {
      case SET_SPECIFICATION:
        //draft = {
        //    ... action.specification
        //}
        draft.nodes = action.specification.nodes;
        draft.edges = action.specification.edges;
        draft.actionCode = action.specification.actionCode;
        break;
      case SET_ACTION_CODE:
        draft.actionCode = action.actionCode;
        break;
      case ADD_NODE:
        // todo validate newNode and current nodes (e.g. not the same id)

        // Add the node to the specification.
        draft.nodes.byIdentifier[action.node.identifier] = action.node;
        draft.nodes.identifiers.push(action.node.identifier);

        // Resolve dependencies such as hierarchical parent child relations:
        let node = draft.nodes.byIdentifier[action.node.identifier];

        // Update the parent.
        if (node.parent !== "") {
          let parentNode = draft.nodes.byIdentifier[node.parent];

          // Add child.
          if (parentNode.children === undefined) {
            parentNode.children = [];
          }
          parentNode.children.push(node.identifier);

          // Set the default entry to the first child if not already set.
          if (not(hasOwnProperty(parentNode.entryPoints, "default"))) {
            parentNode.entryPoints.default = parentNode.children[0];
          }
        }

        // Update children.
        if (node.children.length > 0) {
          for (let index = 0; index < node.children.length; index++) {
            const childNode = draft.nodes.byIdentifier[node.children[index]];
            childNode.parent = node.identifier;
          }

          // Set the default entry to the first child if not already set.
          if (not(hasOwnProperty(node.entryPoints, "default"))) {
            node.entryPoints.default = node.children[0];
          }
        }

        return;
      case REMOVE_NODE:
        // Only node related stuff. Everything else is handled and dispatched in ation. (Such as automatically removing edges)

        // Update children:
        for (let i = 0; i < action.node.children.length; i++) {
          const identifier = action.node.children[i];

          draft.nodes.byIdentifier[identifier].parent = action.node.parent;
        }

        // Update parent:

        if (action.node.parent !== "") {
          draft.nodes.byIdentifier[
            action.node.parent
          ].children = draft.nodes.byIdentifier[
            action.node.parent
          ].children.filter(element => element !== action.node.identifier);
        }

        // Remove node:
        delete draft.nodes.byIdentifier[action.node.identifier];
        draft.nodes.identifiers = draft.nodes.identifiers.filter(
          element => element !== action.node.identifier
        );

        // todo validate that an elemnt was removed

        return;
      case UPDATE_NODE:
        // todo: resolve parent child relations

        // todo valdiate that node exists
        draft.nodes.byIdentifier[action.node.identifier] = action.node;
        return;

      case SET_NODE_NAME:
        draft.nodes.byIdentifier[action.nodeIdentifier].name = action.name;
        return;

      case SET_NODE_SMACH_ACTION:
        draft.nodes.byIdentifier[action.nodeIdentifier].smachAction = {
          identifier: action.actionIdentifier,
          parameters: action.actionParameters
        };
        return;

      case ADD_EDGE:
        (() => {
          // todo validate newNode and current nodes (e.g. not the same id)

          // Add the actual edge.
          draft.edges.byIdentifier[action.edge.identifier] = action.edge;
          draft.edges.identifiers.push(action.edge.identifier);

          // Todo check from to types.

          // Add the edge identifier to the involved nodes.
          const nodeIdentifiers = [
            action.edge.from.identifier,
            action.edge.to.identifier
          ];
          for (let i = 0; i < nodeIdentifiers.length; i++) {
            // If the edge is a transition to itself only add the edge once.
            if (i === 0 || nodeIdentifiers[0] !== nodeIdentifiers[1]) {
              draft.nodes.byIdentifier[nodeIdentifiers[i]].transitions.push(
                action.edge.identifier
              );
            }
          }
        })();

        return;

      case REMOVE_EDGE:
        (() => {
          // Remove the edge from the node transition lists.
          const nodeIdentifiers = [
            action.edge.from.identifier,
            action.edge.to.identifier
          ];
          for (let i = 0; i < nodeIdentifiers.length; i++) {
            draft.nodes.byIdentifier[
              nodeIdentifiers[i]
            ].transitions = draft.nodes.byIdentifier[
              nodeIdentifiers[i]
            ].transitions.filter(element => element !== action.edge.identifier);
          }

          // Finally, remove the edge.
          delete draft.edges.byIdentifier[action.edge.identifier];
          draft.edges.identifiers = draft.edges.identifiers.filter(
            element => element !== action.edge.identifier
          );
        })();

        return;

      case UPDATE_EDGE:
        // todo valdiate that node exists
        draft.edges.byIdentifier[action.edge.identifier] = action.edge;

        return;

      case SET_EDGE_SMACH_OUTCOME:
        draft.edges.byIdentifier[action.edgeIdentifier].if.outcome =
          action.outcome;
        return;

      default:
    }
  })(state, action);
}

// Action Creators
export function setSpecification(specification) {
  return function(dispatch, getState, tree) {
    dispatch({
      type: SET_SPECIFICATION,
      specification
    });

    tree.rehydrate(getState().specification);
  };
}

export function setActionCode(actionCode) {
  return function(dispatch) {
    dispatch({
      type: SET_ACTION_CODE,
      actionCode
    });
  };
}

export function addNode(node) {
  return function(dispatch, getState, tree) {
    let rawNode = JSON.parse(JSON.stringify(node));

    // Create the spatial tree structure.
    const item = {
      minX: rawNode.transform.minX,
      minY: rawNode.transform.minY,
      maxX: rawNode.transform.maxX,
      maxY: rawNode.transform.maxY,
      identifier: rawNode.identifier
    };

    // Determine the parent node and children nodes and augment the node:

    // Get all collisions but exlcude all nodes that only "touch" our current one.
    const collisions = tree.fuzzySearchWithoutBorder(item);

    // Sort the collisions according to their size from big to small.
    collisions.sort((a, b) => {
      // Collisions are always real bigger or real smaller and bigger always enclose
      // smaller ones, so it is enough to check minX
      if (a.minX < b.minX) {
        return -1;
      } else {
        return 1;
      }
    });

    // Iterate over the list. The last element that is bigger than the current node is its parent.
    // All elements that are smaller and have the new parent as its parent are new children.
    const resolveFoundParent = parentIdentifier => {
      // todo we only need the identifier
      rawNode.parent = getState().specification.nodes.byIdentifier[
        parentIdentifier
      ].identifier;

      // toto update parentCandidate
    };
    let parentCandidate = undefined;
    let firstChildCandidateReached = false;
    for (let index = 0; index < collisions.length; index++) {
      const element = collisions[index];

      if (element.minX < item.minX) {
        parentCandidate = element;
        if (index === collisions.length - 1) {
          resolveFoundParent(parentCandidate.identifier);
        }
      } else {
        if (firstChildCandidateReached) {
          if (parentCandidate) {
            resolveFoundParent(parentCandidate.identifier);
          } else {
            // The root is the parent.
            rawNode.parent = "root";
          }
          firstChildCandidateReached = true;
        }

        // We have a new parent. We can reassign all children that are in the list
        // of collisions and that are children of the new parent.
        const elementeNode = getState().specification.nodes.byIdentifier[
          element.identifier
        ];
        if (
          elementeNode.parent === rawNode.parent ||
          elementeNode.parent === undefined
        ) {
          rawNode.children.push(elementeNode.identifier);
          // todo update element
        }
      }
    }

    dispatch({
      type: ADD_NODE,
      node: rawNode
    });

    // Insert a spatial representative of the current tree into the spatial tree structure.
    tree.insert(item);
  };
}

export function removeNode(nodeIdentifier) {
  return function(dispatch, getState, tree) {
    const state = getState();
    const node = state.specification.nodes.byIdentifier[nodeIdentifier];
    // Also remove all connected edges:
    for (let i = 0; i < node.transitions.length; i++) {
      const element = node.transitions[i];

      dispatch(removeEdge(element));
    }

    tree.removeWithIdentifier(node.identifier);

    dispatch({
      type: REMOVE_NODE,
      node
    });
  };
}

export function updateNode(node) {
  // todo check if node with id is present.

  return {
    type: UPDATE_NODE,
    node
  };
}

export function setNodeName(nodeIdentifier, name) {
  return {
    type: SET_NODE_NAME,
    nodeIdentifier,
    name
  };
}

export function setNodeSmachAction(
  nodeIdentifier,
  actionIdentifier,
  actionParameters
) {
  return {
    type: SET_NODE_SMACH_ACTION,
    nodeIdentifier,
    actionIdentifier,
    actionParameters
  };
}

export function addEdge(edge) {
  return function(dispatch) {
    dispatch({
      type: ADD_EDGE,
      edge: edge
    });
  };
}

export function removeEdge(edgeIdentifier) {
  return function(dispatch, getState) {
    // todo check if node with id is present.
    const state = getState();
    const edge = state.specification.edges.byIdentifier[edgeIdentifier];

    dispatch({
      type: REMOVE_EDGE,
      edge: edge
    });
  };
}

export function updateEdge(edge) {
  // todo check if node with id is present.

  return function(dispatch) {
    dispatch({
      type: UPDATE_EDGE,
      edge: edge
    });
  };
}

export function setEdgeSmachOutcome(edgeIdentifier, outcome) {
  return {
    type: SET_EDGE_SMACH_OUTCOME,
    edgeIdentifier,
    outcome
  };
}
