import React from "react";

export const inputContext = React.createContext();
export const inputContextConsumer = inputContext.Consumer;
