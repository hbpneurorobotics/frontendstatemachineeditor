export function getCurrentWorkflowModule(state) {
  return state.workflowModuleSelection.workflowModule;
}
