import { colors } from "./colors.js";

const primaryColorScheme = {
  primary: "#333333",
  secondary: "#333333",
  accent: {
    red: "#333333",
    blue: "#333333"
  },
  feedback: {
    get success() {
      return this.accent.green;
    },
    get failure() {
      return this.accent.red;
    },
    get valid() {
      return this.accent.blue;
    },
    get invalid() {
      return this.accent.green;
    }
  },
  border: {
    primary: "",
    secondary: "",
    ghost: "",
    checked: "",
    disabled: "",
    pressed: "",
    hovered: ""
  },
  text: {
    primary: "#000000",
    secondary: "#000000",
    ghost: "#000000",
    highlighted: "",
    interactive: ""
  },
  layer: {
    background: "#000000",
    staticContent: "#000000",
    interactiveContent: "#000000",
    tool: "#000000",
    foreground: "#000000"
  },
  contrast: {
    low: "#000000",
    mmedium: "#000000",
    high: "#000000",
    highest: "#000000"
  }
};

const currentColorScheme = primaryColorScheme;

export function getPrimaryColor() {
  return currentColorScheme.primaryColor;
}

export function getSecondaryColor() {
  return currentColorScheme.secondaryColor;
}
