# Styling ReadMe

The styling is based on a layer system consisting of three layers:

- **Palette Layer** Specifies concrete color codes for abstract color names.
- **Semantic Layer** Maps the palette to meaning.
- **Element Layer** Applies the meaningful colors to element states.
