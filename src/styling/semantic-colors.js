import { palette } from "./color-palette.js";

const primaryColorScheme = {
  primary: "#333333",
  secondary: "#333333",
  tertiary: "",
  accent: {
    primary: "#333333",
    secondary: "#333333",
    tertiary: "#333333",
    quaternary: "#333333"
  },
  feedback: {
    success: "",
    failure: "#333333",
    valid: "#333333",
    invalid: "#333333",
    hint: "#333333",
    warn: "#333333",
    error: "#333333"
  },
  distance: {
    furthest: "",
    further: "",
    far: "",
    near: "",
    nearer: "",
    nearest: ""
  },
  contrast: {
    low: "#000000",
    mmedium: "#000000",
    high: "#000000",
    highest: "#000000"
  }
};
