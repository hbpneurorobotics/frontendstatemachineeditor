// todo: include light, (lighter) and dark (darker) variations of colors (see https://github.com/OfficeDev/office-ui-fabric-react/blob/master/packages/styling/src/interfaces/IPalette.ts)
// todo: do we need neutral colors?
export const palette = {
  themePrimary: "#222222",
  themeSecondary: "#222222",
  themeTertiary: "#222222",

  themeDarkest: "#222222",
  themeDarker: "#444444",
  themeDark: "#555555",
  themeMiddle: "#666666",
  themeLight: "#888888",
  themeLighter: "#AAAAAA",
  themeLightest: "#CCCCCC",

  themeDarkAlt: "#222222",
  themeLighterAlt: "#222222",

  black: "#222222",
  white: "#222222",

  yellow: "#FFD866",
  orange: "#FC9F72",
  red: "#FF6188",
  magenta: "#000000",
  purple: "#AA9CF0",
  blue: "#78DCE8",
  teal: "#000000",
  green: "#A9DC76"
};
