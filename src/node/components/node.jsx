import React, { useContext } from "react";
import styled from "styled-components";
import { useSelector, useDispatch } from "react-redux";
import { getWidth, getHeight } from "transform-helpers.js";
import { zLayers, getPixelsPerUnit } from "layout";
import {
  Nodelike,
  Grid,
  NodeLikeWrapper,
  ContentWrapper,
  Input
} from "base/components";
import { EntryPoints } from "./entry-points.jsx";
import { SmachAction } from "./smach-action.jsx";
import { setNodeName } from "specification";
import {
  inputContext,
  useHoveredTargetDetector,
  useHoveredTarget,
  dispatchInputEvent
} from "input";
import { getSelectedPrimaryTool } from "selectors.js";

const StyledNodeLikeWrapper = styled(NodeLikeWrapper)`
  z-index: ${props =>
    zLayers.nodes +
    10000 -
    getWidth(props.transform) * getHeight(props.transform)};
`;

const GridWrapper = styled.div`
  grid-area: middle;
  min-height: 0;
  min-width: 0;
  pointer-events: none;
`;

export const FullAreaItem = styled.div`
  grid-column-start: column-start;
  grid-column-end: column-end;
  grid-row-start: row-start;
  grid-row-end: row-end;
`;

const neutralBorderColor = "#78DCE8";
const neutralBackgroundColor = "#d6eaed";
const negativeBorderColor = "#FF6188";
const negativeBackgroundColor = "#f2d2da";

const StyledNodelike = styled(Nodelike).attrs(props => ({
  hoverBorderColor:
    props.mood === "negative" ? negativeBorderColor : neutralBorderColor,
  hoverBackgroundColor:
    props.mood === "negative" ? negativeBackgroundColor : neutralBackgroundColor
}))`
  box-shadow: -3px 3px 3px 0px rgba(0, 0, 0, 0.25);
  ${({ hovered, hoverBorderColor, hoverBackgroundColor }) =>
    hovered &&
    `
        background-color: ${hoverBackgroundColor};
        border-color: ${hoverBorderColor};
  `}
`;

const Name = styled(Input).attrs(props => {
  const pixelsPerUnit = getPixelsPerUnit();

  return {
    pixelsPerUnit
  };
})`
  border-radius: 4px;
  margin: ${props => props.pixelsPerUnit / 8}px;
  grid-area: row-top-start / column-center-start / row-top-end /
    column-right-end;
  min-width: 1em;
  overflow: hidden;
  place-self: stretch stretch;
  z-index: ${zLayers.nodeContent};
`;

const MiddleLines = styled.div.attrs(props => {
  const pixelsPerUnit = getPixelsPerUnit();

  return {
    pixelsPerUnit
  };
})`
  grid-area: middle;
  display: grid;
  grid-auto-rows: ${props => props.pixelsPerUnit}px;
`;

const ActionWrapper = styled.div`
  grid-area: 1 / 1 / 3 / 2;
`;

const ResizeToolWrapper = styled.div.attrs(props => {
  const pixelsPerUnit = getPixelsPerUnit();

  return {
    pixelsPerUnit
  };
})`
  grid-column-start: column-start;
  grid-column-end: column-end;
  grid-row-start: row-start;
  grid-row-end: row-end;
  z-index: ${zLayers.nodeContent + 100};
  display: grid;
  grid-template-columns: ${props =>
    ` ${props.pixelsPerUnit / 2}px auto ${props.pixelsPerUnit / 2}px`};
  grid-template-rows: ${props =>
    `${props.pixelsPerUnit / 2}px auto  ${props.pixelsPerUnit / 2}px`};
  grid-template-areas:
    "top-left        top             top-right       "
    "left            middle          right           "
    "bottom-left     bottom          bottom-right    ";
`;

const BorderWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const TopLeftWrapper = styled(BorderWrapper)`
  grid-area: top-left;
`;
const TopWrapper = styled(BorderWrapper)`
  grid-area: top;
`;
const TopRightWrapper = styled(BorderWrapper)`
  grid-area: top-right;
`;
const RightWrapper = styled(BorderWrapper)`
  grid-area: right;
`;
const BottomRightWrapper = styled(BorderWrapper)`
  grid-area: bottom-right;
`;
const BottomWrapper = styled(BorderWrapper)`
  grid-area: bottom;
`;
const BottomLeftWrapper = styled(BorderWrapper)`
  grid-area: bottom-left;
`;
const LeftWrapper = styled(BorderWrapper)`
  grid-area: left;
`;

const StyledResizeHandle = styled.div.attrs(props => {
  const pixelsPerUnit = getPixelsPerUnit();

  return {
    pixelsPerUnit
  };
})`
  width: ${props => props.pixelsPerUnit / 2}px;
  height: ${props => props.pixelsPerUnit / 2}px;
  border-radius: 999px;
  grid-column-start: 1;
  grid-column-end: 2;
  grid-row-start: ${props => props.index + 1};
  grid-row-end: ${props => props.index + 2};
  background-color: #aaa;
  cursor: pointer;
  z-index: ${zLayers.gui + 10};
  &:hover {
    background-color: #bbb;
  }
  &.active {
    background-color: orange;
  }
`;

export function Node(props) {
  const dispatch = useDispatch();
  const specification = props.specification;
  const anchor = props.anchor;
  const ic = useContext(inputContext);
  const selectedPrimaryTool = useSelector(getSelectedPrimaryTool);

  const handleChange = event => {
    dispatch(setNodeName(specification.identifier, event.target.value));
  };

  const { mouseEnterHandler, mouseLeaveHandler } = useHoveredTargetDetector(
    ic.layer,
    props.specification.identifier,
    "node"
  );

  const hover = useHoveredTarget(ic.layer);
  const hovered = hover.current.identifier === props.specification.identifier;

  const dispatchNodeDownEvent = function() {
    dispatchInputEvent(ic.layer, "NODE_DOWN", {
      nodeSpecification: specification
    });
  };
  const dispatchResizeHandleEvent = function(direction) {
    dispatchInputEvent(ic.layer, "RESIZE_HANDLE_DOWN", {
      direction: direction,
      nodeSpecification: specification
    });
  };

  return (
    <StyledNodeLikeWrapper
      className={hovered ? "hovered" : ""}
      anchor={anchor}
      transform={specification.transform}
      onMouseEnter={mouseEnterHandler}
      onMouseLeave={mouseLeaveHandler}
      onMouseDown={() => {
        dispatchNodeDownEvent();
      }}
    >
      {selectedPrimaryTool.identifier === "resizeNode" && hovered && (
        <ResizeToolWrapper>
          <TopLeftWrapper>
            <StyledResizeHandle
              onMouseDown={() => {
                dispatchResizeHandleEvent("top-left");
              }}
            />
          </TopLeftWrapper>
          <TopWrapper>
            <StyledResizeHandle
              onMouseDown={() => {
                dispatchResizeHandleEvent("top");
              }}
            />
          </TopWrapper>
          <TopRightWrapper>
            <StyledResizeHandle
              onMouseDown={() => {
                dispatchResizeHandleEvent("top-right");
              }}
            />
          </TopRightWrapper>
          <RightWrapper>
            <StyledResizeHandle
              onMouseDown={() => {
                dispatchResizeHandleEvent("right");
              }}
            />
          </RightWrapper>
          <BottomRightWrapper>
            <StyledResizeHandle
              onMouseDown={() => {
                dispatchResizeHandleEvent("bottom-right");
              }}
            />
          </BottomRightWrapper>
          <BottomWrapper>
            <StyledResizeHandle
              onMouseDown={() => {
                dispatchResizeHandleEvent("bottom");
              }}
            />
          </BottomWrapper>
          <BottomLeftWrapper>
            <StyledResizeHandle
              onMouseDown={() => {
                dispatchResizeHandleEvent("bottom-left");
              }}
            />
          </BottomLeftWrapper>
          <LeftWrapper>
            <StyledResizeHandle
              onMouseDown={() => {
                dispatchResizeHandleEvent("left");
              }}
            />
          </LeftWrapper>
        </ResizeToolWrapper>
      )}
      <ContentWrapper>
        <GridWrapper>
          <Grid show={selectedPrimaryTool?.hover.grid && hovered} />
        </GridWrapper>
        <Name type="text" value={specification.name} onChange={handleChange} />
        <MiddleLines>
          <ActionWrapper>
            {(specification.children === undefined || (specification.children.length === 0 && specification.name !== "START")) && 
            <SmachAction
              identifier={specification.identifier}
              action={specification.smachAction}
            />}
          </ActionWrapper>
        </MiddleLines>
        <EntryPoints entryPoints={specification.entryPoints} />
      </ContentWrapper>
      <StyledNodelike
        hovered={selectedPrimaryTool?.hover.node && hovered}
        mood={selectedPrimaryTool?.hover.mood}
      />
    </StyledNodeLikeWrapper>
  );
}
