import React, { useContext, useRef } from "react";
import { useDispatch } from "react-redux";
import { inputContext, useDrawGestureEvents } from "input";
import { removeNode, removeEdge } from "specification";
import { dispatchClearTarget } from "input/modules/hovered-target.js";

export function EraseTool(props) {
  const dispatch = useDispatch();
  const ic = useContext(inputContext);

  const initialContextState = {
    start: {
      hoverTarget: {}
    },
    latest: {
      hoverTarget: {}
    }
  };
  const gestureContext = useRef(initialContextState);
  
  useDrawGestureEvents(
    ic.layer,
    (event, context) => {
      // start
      gestureContext.current = {
        ...gestureContext.current,
        start: {
          hoverTarget: event.detail.hoverTarget
        }
      };
    },
    (event, context) => {
      // update
    },
    (event, context) => {
      // complete
      gestureContext.current = {
        ...gestureContext.current,
        latest: {
          hoverTarget: event.detail.hoverTarget
        }
      };

      // Context is a erasable object and mouse down was on the same object as the mouse up
      if (
        gestureContext.current.start.hoverTarget.identifier !== "" &&
        gestureContext.current.start.hoverTarget.type !== "" &&
        gestureContext.current.start.hoverTarget.identifier ===
          gestureContext.current.latest.hoverTarget.identifier
      ) {
        if (gestureContext.current.latest.hoverTarget.type === "node") {
          dispatch(
            removeNode(gestureContext.current.start.hoverTarget.identifier)
          );
        } else if (gestureContext.current.latest.hoverTarget.type === "edge") {
          dispatch(
            removeEdge(gestureContext.current.start.hoverTarget.identifier)
          );
        }
      }

      dispatchClearTarget(ic.layer);
      gestureContext.current = initialContextState;
    },
    () => {
      // cancel
      gestureContext.current = initialContextState;
    }
  );

  return <div></div>;
}
