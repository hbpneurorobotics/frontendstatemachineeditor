import { NODE_MIN_WIDTH, NODE_MIN_HEIGHT } from "specification/constants.js";

export function validateTransform(transform, tree) {
  // Check size:
  const size = {
    x: transform.maxX - transform.minX,
    y: transform.maxY - transform.minY
  };
  if (size.x < NODE_MIN_WIDTH || size.y < NODE_MIN_HEIGHT) {
    return false;
  }

  // Check if startContext is also endContext AND check if it is completely inside or outsite of all nodes.

  // Get all collisions
  let collisions = tree.fuzzySearchWithoutBorder(transform);

  if (collisions.length > 0) {
    return false;
  }

  // Todo: old code for hierarchical states.
  /*

    // Get all collisions
    let collisions = tree.search(transform)

    for (let i = 0; i < collisions.length; i++) {
        const other = collisions[i];

        if(validateSpatialRelationBetweenTransforms(transform, other) === false){
            return false;
        }
    }*/
  return true;
}

export function validateSpatialRelationBetweenTransforms(a, b) {
  let left = false;
  let right = false;
  let above = false;
  let below = false;
  let onlyOneSide = false; // Only one side is intersecting -> both are just touching each other.

  // Check all directions if a is _ (of) b

  // Is a left of b?
  if (a.minX < b.minX || a.maxX < b.minX) {
    left = true;

    if (a.maxX === b.minX) {
      onlyOneSide = true;
    }
  }

  // Is a right of b?
  if (a.minX > b.maxX || a.maxX > b.maxX) {
    right = true;
    if (a.minX === b.maxX) {
      onlyOneSide = true;
    }
  }

  // Is a above b?
  if (a.minY < b.minY || a.maxY < b.minY) {
    above = true;
    if (a.maxY === b.minY) {
      onlyOneSide = true;
    }
  }

  // Is a below b?
  if (a.minY > b.maxY || a.maxY > b.maxY) {
    below = true;
    if (a.minY === b.maxY) {
      onlyOneSide = true;
    }
  }

  // Determine if the spatial relation between a and b is valid:

  if (!left && !right && !below && !above) {
    // A is completely inside.

    if (
      b.minX === a.minX ||
      b.maxX === a.maxX ||
      b.minY === a.minY ||
      b.maxY === a.maxY
    ) {
      // When A is inside, no equal sides are allowed.
      return false;
    } else {
      return true;
    }
  } else if (left && right && below && above) {
    // A is outside and encloses the element.
    // Equal sides are allowed.
    return true;
  } else if (onlyOneSide) {
    // A is outside but only on one side.
    // This is the least common case because it is only detected when both "touch" each other.
    return true;
  } else {
    return false;
  }
}

export function createTransform(
  fromGridPosition,
  toGridPosition,
  anchors = []
) {
  if (anchors.length === 1) {
    // One edge is fix.
    return {
      minX: Math.min(toGridPosition.x, anchors[0].x),
      maxX: Math.max(toGridPosition.x, anchors[0].x) + 1,
      minY: Math.min(toGridPosition.y, anchors[0].y),
      maxY: Math.max(toGridPosition.y, anchors[0].y) + 1
    };
  } else if (anchors.length === 2) {
    // One variable side or all locked.
    return {
      minX:
        anchors[0].x === anchors[1].x
          ? Math.min(toGridPosition.x, anchors[0].x, anchors[1].x)
          : Math.min(anchors[0].x, anchors[1].x),
      maxX:
        (anchors[0].x === anchors[1].x
          ? Math.max(toGridPosition.x, anchors[0].x, anchors[1].x)
          : Math.max(anchors[0].x, anchors[1].x)) + 1,
      minY:
        anchors[0].y === anchors[1].y
          ? Math.min(toGridPosition.y, anchors[0].y, anchors[1].y)
          : Math.min(anchors[0].y, anchors[1].y),
      maxY:
        (anchors[0].y === anchors[1].y
          ? Math.max(toGridPosition.y, anchors[0].y, anchors[1].y)
          : Math.max(anchors[0].y, anchors[1].y)) + 1
    };
  } else {
    // Invalid anchors or no at all.
    return {
      minX: Math.min(fromGridPosition.x, toGridPosition.x),
      maxX: Math.max(fromGridPosition.x, toGridPosition.x) + 1,
      minY: Math.min(fromGridPosition.y, toGridPosition.y),
      maxY: Math.max(fromGridPosition.y, toGridPosition.y) + 1
    };
  }
}
