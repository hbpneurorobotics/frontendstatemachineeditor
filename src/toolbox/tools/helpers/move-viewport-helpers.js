import { getPixelsPerUnit } from "layout";

export function getSnappedOffset(offset) {
  const pixelsPerUnit = getPixelsPerUnit();
  return {
    x: Math.round(offset.x / pixelsPerUnit) * pixelsPerUnit,
    y: Math.round(offset.y / pixelsPerUnit) * pixelsPerUnit
  };
}
