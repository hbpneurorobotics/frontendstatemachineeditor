import React, { useState, useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import styled from "styled-components";
import { Node } from "./../../node/components";
import { Grid } from "./../../base/components";
import { DiscretePointer } from "./../../pointers/components";
import { Toolbar } from "toolbar/components/index.js";
import { Toolbox } from "toolbox/components/index.js";
import { EdgeCanvas } from "edges/components/index.js";
import {
  inputContext,
  useDrawGestureDetector,
  usePointerPositionDetector
} from "input";
import { updateSpaceRect } from "../";
import {
  getNodes,
  getSpaceOffset,
  getTemporalSpaceOffset
} from "selectors";

let uuidv4 = require("uuid/v4");

const StyledSpaceWrapper = styled.div`
  position: relative;
  overflow: hidden;
  width: 100%;
  height: 100%;
`;

const StyledSpace = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  transform: ${props => `translate(${props.offset.x}px, ${props.offset.y}px)`};
`;

export function Space() {
  const nodes = useSelector(getNodes);
  const spaceOffset = useSelector(getSpaceOffset);
  const temporalSpaceOffset = useSelector(getTemporalSpaceOffset);
  const moveViewportIsActive =
    temporalSpaceOffset.x !== 0 || temporalSpaceOffset.y !== 0;

  const [spaceIdentifier] = useState(uuidv4());
  const [spaceRect, setSpaceRect] = useState({
    left: 0,
    top: 0,
    witdh: 0,
    height: 0
  });

  const inputEventLayer = `${spaceIdentifier}/input`;
  const drawGestureCallbacks = useDrawGestureDetector(inputEventLayer);

  const dispatch = useDispatch();
  let myRef = useRef();

  // Connect the input utilities to the space element:

  useEffect(() => {
    const handleResize = () => {
      const bodyRect = document.body.getBoundingClientRect();
      const elementRect = myRef.current.getBoundingClientRect();
      const temporalSpaceRect = {
        left: elementRect.left - bodyRect.left,
        top: elementRect.top - bodyRect.top,
        width: elementRect.width,
        height: elementRect.height
      };

      if (spaceRect !== temporalSpaceRect) {
        setSpaceRect(temporalSpaceRect);
        dispatch(updateSpaceRect(temporalSpaceRect));
      }
    };

    // Call once for initial space rect.
    handleResize();

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  usePointerPositionDetector(inputEventLayer, spaceRect, spaceOffset);

  let nodeArray = [];

  nodes.identifiers.forEach(key => {
    nodeArray.push(
      <Node
        key={key}
        specification={nodes.byIdentifier[key]}
        anchor={spaceRect}
      ></Node>
    );
  });

  return (
    <StyledSpaceWrapper
      onMouseDown={event => {
        drawGestureCallbacks.mouseDownHandler(event);
      }}
      onMouseUp={event => {
        drawGestureCallbacks.mouseUpHandler(event);
      }}
      onMouseLeave={event => {
        drawGestureCallbacks.mouseLeaveHandler(event);
      }}
      ref={myRef}
    >
      <inputContext.Provider
        value={{
          layer: inputEventLayer
        }}
      >
        <StyledSpace
          offset={{
            x: spaceOffset.x + temporalSpaceOffset.x,
            y: spaceOffset.y + temporalSpaceOffset.y
          }}
        >
          <Grid
            offset={{
              x: -spaceOffset.x,
              y: -spaceOffset.y
            }}
            show={moveViewportIsActive === false}
          />

          {nodeArray}

          <DiscretePointer />

          <Toolbox />

          <EdgeCanvas
            spaceRect={spaceRect}
            offset={{
              x: -spaceOffset.x,
              y: -spaceOffset.y
            }}
            show={moveViewportIsActive === false}
          />
        </StyledSpace>
        <Toolbar />
      </inputContext.Provider>
    </StyledSpaceWrapper>
  );
}
