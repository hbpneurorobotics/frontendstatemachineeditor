export function hasOwnProperty(object, property) {
  return {}.hasOwnProperty.call(object, property);
}

export function not(boolean) {
  return boolean === false;
}
