export function getWidth(transform) {
  return transform.maxX - transform.minX;
}

export function getHeight(transform) {
  return transform.maxY - transform.minY;
}

export function getXPosition(transform) {
  return transform.minX;
}

export function getYPosition(transform) {
  return transform.minY;
}
