import React from "react";
import "./App.css";
import { Provider } from "react-redux";
import { store } from "./configure-store.js";
import { WorkflowModuleSelection } from "./workflow-module-selection/components";
import "./styles.css";

function App() {
  const divStyle = {
    width: "100%",
    height: "100%"
  };

  return (
    <div style={divStyle}>
      <Provider store={store}>
        <WorkflowModuleSelection />
      </Provider>
    </div>
  );
}

export default App;
