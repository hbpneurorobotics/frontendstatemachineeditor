import { useState } from "react";

export function usePointerVisibility(pointer, focusedNode) {
  const [visibility, setVisibility] = useState(true);
  let newVisibility = visibility;

  if (focusedNode) {
    const hoversLeftBorder = pointerPosition.x === focusedNode.transform.minX;
    const hoversRightBorder =
      pointerPosition.x === focusedNode.transform.maxX - 1;
    const hoversTopBorder = pointerPosition.y === focusedNode.transform.minY;
    const hoversSecondLineTop =
      pointerPosition.y === focusedNode.transform.minY + 1;
    const hoversBottomBorder =
      pointerPosition.y === focusedNode.transform.maxY - 1;

    if (
      hoversLeftBorder ||
      hoversRightBorder ||
      hoversTopBorder ||
      hoversSecondLineTop ||
      hoversBottomBorder
    ) {
      newVisibility = false;
    }
  }

  if (newVisibility !== visibility) {
    setVisibility(newVisibility);
  }
}
